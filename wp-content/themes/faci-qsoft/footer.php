  <footer class="main-footer">
                <!-- Footer widgets -->
                

  <!-- Footer widgets -->
      <div class="big-footer box">
        <div class="container">
          <div class="footer-sidebar row">

            <div class="col-md-4 col-sm-4 widget">
                            <?php dynamic_sidebar('footer-column1-widgets'); ?>
            </div>

            
            <div class="col-md-4 col-sm-4 widget post-widget">
                 
            <h4>
                Theo dõi chúng tôi trên
            </h4>
            <ul class="inline-list social-links">
                <li>
                    <a  
                        target="_blank" 
                        href="<?php echo print_option('facebook-url'); ?>"  
                        class="social-facebook shape-square font-2x soft-corners">
                        <i class="fa fa-facebook"></i>
                    </a>
                </li>
                <li>
                    <a 
                        target="_blank" 
                        href="<?php echo print_option('facebook-url'); ?>"  
                        class="social-skype shape-square font-2x soft-corners">
                        <i class="fa fa-instagram"></i>
                    </a>
                </li>
                <li>
                    <a 
                        target="_blank" 
                        href="<?php echo print_option('facebook-url'); ?>"  
                        class="social-rss shape-square font-2x soft-corners">
                        <i class="fa fa-youtube"></i>
                    </a>
                </li>
                <li>
                    <a 
                        target="_blank" 
                        href="<?php echo print_option('facebook-url'); ?>"  
                        class="social-google shape-square font-2x soft-corners">
                        <i class="fa fa-google-plus"></i>
                    </a>
                </li>
            </ul>

            <div class="subscribe-wrapper">
                  
            <div class="subscribe-form row">
                <div class="col-md-12">
                    <?php dynamic_sidebar('footer-subscribe-widgets'); ?>
                   
                </div>
                
            </div>
              </div>              
            </div>
            <div class="col-md-4 col-sm-4 widget">
               <h4>
                                Thư viện ảnh
                            </h4>
                            <ul class="flickr-widget clean-list row">
                                <?php 
                                    $q_portfolio= new WP_Query(
                                        array(
                                            'post_type'=>'portfolio',
                                            'posts_per_page'=>6
                                        )
                                    );
                                    if($q_portfolio->have_posts()):while($q_portfolio->have_posts()):$q_portfolio->the_post();
                                ?>
                                <li class="col-xs-3 col-sm-4 col-md-4">
                                    <a class="flickr-link" href="<?php the_permalink(); ?>" >
                                         <?php 
                                            if(has_post_thumbnail( ))
                                                the_post_thumbnail('large',array('alt'=>get_the_title(),'class'=>'flickr-img'));
                                            else echo ' <img src="'.get_theme_mod("img_error").'" alt="image"  class="flickr-img"/>';
                                        ?>
                                    </a>
                                </li>
                                <?php  
                                    endwhile;
                                    wp_reset_postdata();
                                    endif;

                                ?> 
                            </ul>
                                

              <div class="usefull-links">
                 <h4>
                                Link hữu ích
                            </h4>
                            <?php 
                                $args_f = array(
                                    'theme_location' => 'bot-menu',
                                    'container' => '',
                                    'items_wrap' => '<ul id = "menu_footer" class = "%2$s">%3$s</ul>',
                                );
                                wp_nav_menu( $args_f );
                            ?>
                      </div>
                    </div>
                  </div><!-- /.row -->
                </div><!-- /.container -->
              </div><!-- /.big-footer -->
                <!-- /.big-footer -->
                <!-- Copyright section -->
                <div class="small-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                            <?php dynamic_sidebar('footer-copyright-widgets'); ?>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
                <!-- /.small-footer -->
            </footer>
            <!-- /.main-footer -->
        </div>
        <!-- /.boxed-view -->

        <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js" ></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/vinhome/bootstrap-datepicker.min.js"></script>
        
        
        <a id='backTop'><i class="fa fa-angle-up"></i></a>
        <script>
       
            $(document).ready(function (e) {
                   
                
                $(".modal").css('display', 'block');
                $(".button-close").click(function (e) {
                    $(".modal").css('display', 'none');
                });
                $("#menu_footer li").addClass('col-md-4 col-sm-4 col-xs-6');
                $(".es_textbox_class").attr("placeholder", "Nhập địa chỉ E-mail");
                $(".es_textbox_button").attr("value", "Đăng ký");

            });
        </script>
    </form>
    <?php wp_footer(); ?>
</body>
</html>