<?php get_header(); ?>
            <!-- main content -->
            <section class="slider-box">
                <?php echo do_shortcode( '[rev_slider homeslide]' ); ?>
            </section>
            <!-- /.slider-box -->
            
            <?php include(get_template_directory().'/multi-search.php'); ?>
           
            <!-- /.book-form -->
            <section class="box mb-30">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-dark-blue text-center fancy-heading vc_custom_heading">
                                <h1>
                                    Bất động sản cho thuê
                                </h1>
                                <hr class="center-me" style="color: inherit; width: 30%;">
                            </div>

                        </div>
                    </div>
                    <div class="container">
             
                    
                    <ul class="clean-list rooms-items row">
                        <?php 
                            
                            if($q_du_an->have_posts()):while($q_du_an->have_posts()):$q_du_an->the_post();
                        ?>
                                <li class="col-md-3 col-sm-6">
                                <div class="box-rooms">
                                    <figure>
                                        <a href="<?php the_permalink(); ?>" >
                                            <?php 
                                                if(has_post_thumbnail( ))
                                                    the_post_thumbnail('large',array('alt'=>get_the_title()));
                                                else echo ' <img src="'.get_theme_mod("img_error").'" alt="image" />';
                                            ?>
                                        </a>
                                    </figure>
                                    <div class="rooms-description">
                                        <h3 class="title-rooms">
                                            <a href="<?php the_permalink(); ?>"  class="hover-text-aquablue"><?php the_title(); ?></a></h3>
                                        <p>
                                            <?php the_faci_excerpt(200); ?>
                                        </p>
                                    </div>
                                </div>
                            </li>
                        <?php  
                            endwhile;
                            wp_reset_postdata();
                            endif;

                        ?> 
                         

                    </ul>


                    <!-- /.row -->
                </div>
            </section>
            <section class="box border-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-dark-blue text-center fancy-heading vc_custom_heading">
                                <h1>
                                    Tiện ích
                                </h1>
                                <hr class="center-me" style="color: inherit; width: 30%;">
                            </div>

                        </div>
                    </div>
                    
                    <ul class="clean-list facilities-items text-center row">
                        <?php 
                            $q_tien_ich= new WP_Query(
                                array(
                                    'cat'=>1,
                                    'posts_per_page'=>7
                                )
                            );
                            if($q_tien_ich->have_posts()):while($q_tien_ich->have_posts()):$q_tien_ich->the_post();
                        ?>
                        <li class="col-md-3 col-sm-4">
                            <div class="facility-item">
                                <div class="shape-square aquablue">
                                    <figure>
                                        <a href="<?php the_permalink(); ?>" >
                                            <?php 
                                                if(has_post_thumbnail( ))
                                                    the_post_thumbnail('large',array('alt'=>get_the_title()));
                                                else echo ' <img src="'.get_theme_mod("img_error").'" alt="image" />';
                                            ?>
                                        </a>
                                    </figure>
                                </div>
                                <span class="uppercase font-300 text-white facility-text"><?php the_title(); ?></span>
                            </div>
                        </li>
                        <?php  
                            endwhile;
                            wp_reset_postdata();
                            endif;

                        ?> 
                    </ul>

                    <!-- /.row -->

                </div>
                <!-- /.container -->
            </section>
            <!-- /.facilities -->
            <section style="">
                
                <div class="container">
                    <div class="row">
                        
                        <style type="text/css" media="screen">
                            .glyphicon-lg{font-size:3em}
                            .blockquote-box{border-right:5px solid #E6E6E6;margin-bottom:25px}
                            .blockquote-box .square{width:120px;min-height:50px;margin-right:22px;text-align:center!important;padding:20px 0}
                            .blockquote-box.blockquote-primary{border-color:#357EBD}
                            .blockquote-box.blockquote-primary .square{background-color:#428BCA;color:#FFF}
                            .blockquote-box.blockquote-success{border-color:#4CAE4C}
                            .blockquote-box.blockquote-success .square{background-color:#5CB85C;color:#FFF}
                            .blockquote-box.blockquote-info{border-color:#46B8DA}
                            .blockquote-box.blockquote-info .square{background-color:#5BC0DE;color:#FFF}
                            .blockquote-box.blockquote-warning{border-color:#EEA236}
                            .blockquote-box.blockquote-warning .square{background-color:#F0AD4E;color:#FFF}
                            .blockquote-box.blockquote-danger{border-color:#D43F3A}
                            .blockquote-box.blockquote-danger .square{background-color:#D9534F;color:#FFF}
                        </style>
                        <div class="col-md-6">
                            <div class="text-dark-blue text-center fancy-heading vc_custom_heading">
                                <h2>
                                    TIN MỚI
                                </h2>
                                <hr class="center-me" style="color: inherit; width: 30%;">
                            </div>
                            <?php 
                                $q_new= new WP_Query(
                                    array(
                                        'post_type'=>'post',
                                        'posts_per_page'=>3,
                                        'cat'=>2
                                    )
                                );
                                if($q_new->have_posts()):while($q_new->have_posts()):$q_new->the_post();
                            ?>
                            <div class="blockquote-box clearfix">
                                <div class="square pull-left">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                    <?php 
                                        if(has_post_thumbnail( ))
                                            the_post_thumbnail('large',array('alt'=>get_the_title()));
                                        else echo ' <img src="'.get_theme_mod("img_error").'" alt="image" />';
                                    ?>
                                </a>
                                </div>
                                <h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
                                <p>
                                    <?php the_faci_excerpt(); ?>
                                </p>
                            </div>
                             <?php  
                                endwhile;
                                wp_reset_postdata();
                                endif;
                            ?> 
                           
                        </div>
                        <div class="col-md-6">
                        <div class="text-dark-blue text-center fancy-heading vc_custom_heading">
                                <h2>
                                    CẢM NHẬN KHÁCH HÀNG
                                </h2>
                                <hr class="center-me" style="color: inherit; width: 30%;">
                            </div>
                        <style type="text/css" media="screen">
                            @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css);
                            .nav.nav-justified > li > a { position: relative; }
                            .nav.nav-justified > li > a:hover,
                            .nav.nav-justified > li > a:focus { background-color: transparent; }
                            .nav.nav-justified > li > a > .quote {
                                position: absolute;
                                left: 0px;
                                top: 0;
                                opacity: 0;
                                width: 30px;
                                height: 30px;
                                padding: 5px;
                                background-color: #13c0ba;
                                border-radius: 15px;
                                color: #fff;  
                            }
                            .nav.nav-justified > li.active > a > .quote { opacity: 1; }
                            .nav.nav-justified > li > a > img { box-shadow: 0 0 0 5px #13c0ba; }
                            .nav.nav-justified > li > a > img { 
                                max-width: 100%; 
                                opacity: .3; 
                                -webkit-transform: scale(.8,.8);
                                        transform: scale(.8,.8);
                                -webkit-transition: all 0.3s 0s cubic-bezier(0.175, 0.885, 0.32, 1.275);
                                        transition: all 0.3s 0s cubic-bezier(0.175, 0.885, 0.32, 1.275);
                            }
                            .nav.nav-justified > li.active > a > img,
                            .nav.nav-justified > li:hover > a > img,
                            .nav.nav-justified > li:focus > a > img { 
                                opacity: 1; 
                                -webkit-transform: none;
                                        transform: none;
                                -webkit-transition: all 0.3s 0s cubic-bezier(0.175, 0.885, 0.32, 1.275);
                                        transition: all 0.3s 0s cubic-bezier(0.175, 0.885, 0.32, 1.275);
                            }
                            .tab-pane .tab-inner { padding: 30px 0 20px; }

                            @media (min-width: 768px) {
                                .nav.nav-justified > li > a > .quote {
                                    left: auto;
                                    top: auto;
                                    right: 20px;
                                    bottom: 0px;
                                }  
                            }    
                        </style>
                            <div class="[ row ]">
                            <div class="[ col-xs-12 col-sm-8  col-md-12  ]" role="tabpanel">
                                <div class="[ col-xs-4 col-sm-12 ]">
                                    <!-- Nav tabs -->
                                    <ul class="[ nav nav-justified ]" id="nav-tabs" role="tablist">
                                    <?php
                                    $q_testimo= new WP_Query(
                                            array(
                                                'post_type'=>'testimonials',
                                                'posts_per_page'=>4,
                                            )
                                        );
                                        if($q_testimo->have_posts()): $i=0; while($q_testimo->have_posts()):$q_testimo->the_post() ;$i++;
                                    ?>
                                        <li role="presentation" class="<?php if($i==1) echo 'active'; ?>">
                                            <a href="#dustin" aria-controls="dustin" role="tab" data-toggle="tab">
                                                <?php the_post_thumbnail('large',array('class'=>'img-circle')); ?>
                                                <span class="quote"><i class="fa fa-quote-left"></i></span>
                                            </a>
                                        </li>
                                     <?php  
                                        endwhile;
                                        wp_reset_postdata();
                                        endif;
                                    ?>    
                                    </ul>
                                </div>
                                <div class="[ col-xs-8 col-sm-12 ]">
                                    <!-- Tab panes -->
                                    <div class="tab-content" id="tabs-collapse">  
                                    <?php  if($q_testimo->have_posts()): $i=0;while($q_testimo->have_posts()):$q_testimo->the_post(); $i++;?>          
                                        <div role="tabpanel" class="tab-pane fade in <?php if($i==1) echo ''; ?>" id="dustin">
                                            <div class="tab-inner">                    
                                                <?php echo get_the_content(); ?>
                                                <br/><br/>
                                                <p><strong class="text-uppercase"><?php the_title(); ?></strong></p>
                                            </div>
                                        </div>
                                        <?php  
                                            endwhile;
                                            wp_reset_postdata();
                                            endif;
                                        ?>   
                                    </div>
                                </div>        
                            </div>
                        </div>


                        </div>
                    </div>
                </div>

            </section>
<?php get_footer(); ?>     
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               