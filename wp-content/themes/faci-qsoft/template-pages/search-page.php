<?php 
/*
 * template name:search
*/
?>
<?php get_header(); ?>
<?php include(get_template_directory().'/multi-search.php'); ?>
<!-- /.book-form -->
<!-- main content -->
<section class="box">
  <div class="container">
        <div class="row">
            <div class="col-md-12">
                    <div class="text-dark-blue text-center fancy-heading vc_custom_heading">
                                <h1>
                                <?php the_title(); ?>
                                </h1><hr class="center-me" style="color: inherit; width:30%;">
                            </div>
                            <ul class="clean-list row gallery-items">
                                <?php  
                                    $q_du_an= new WP_Query(
                                        array(
                                            'post_type'=>'portfolio',
                                        )
                                    );
                                    if($q_du_an->have_posts()):while($q_du_an->have_posts()):$q_du_an->the_post();
                                ?>   
                                <li class="text-center">
                                    <div class="shape-square middle-corners">
                                        
                                          
                                         <a href='<?php the_permalink(); ?>'>
                                            <?php 
                                                if(has_post_thumbnail( ))
                                                    the_post_thumbnail('large',array('alt'=>get_the_title()));
                                                else echo ' <img src="'.get_theme_mod("img_error").'" alt="image" />';
                                            ?>
                                        </a>
                                    </div>
                                    <h4 class="title-gallery">
                                        <a href="<?php the_permalink(); ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </h4>
                                </li>
                                 <?php  
                                    endwhile;
                                    wp_reset_postdata();
                                    endif;
                                ?> 
                            </ul>
            </div>
        </div>
    </div>

</section>
<!-- /.box -->
<?php get_footer(); ?>
