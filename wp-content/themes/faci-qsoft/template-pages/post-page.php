<?php 
/*
 * template name: Post new
*/
?>
<?php get_header(); ?>
<?php include(get_template_directory().'/multi-search.php'); ?>
<!-- /.book-form -->
<!-- main content -->
<section class="box">
    <div class="container">
	<div class="row">
		<div id="contLeft" class="col-md-9 col-sm-9">
                <div class="row">
                    <div class="col-md-12">
                          <div class="text-dark-blue text-center fancy-heading vc_custom_heading">
                                <h1>
                                 <?php the_title(); ?>
                                </h1><hr class="center-me" style="color: inherit; width:30%;">
                            </div>
                    
                    </div>
                </div>
                <!-- /.row -->
                
                <?php 
                	if(have_posts()):while(have_posts()):the_post();
                		the_content();
                    endwhile;
                	endif;

                ?>
                <form id="frmKygui"> 
                <div id="k_result" class="col-md-12"></div>  
                <div id="k_form"  class="col-md-12">
                     
                    <p class="col-md-6">
                        <input class="form-control" id="k_name" type="text" name="k_name" placeholder="Họ và tên (*)" />
                    </p>
                    <p class="col-md-6">
                        <input class="form-control" id="k_phone" type="tel" name="k_phone" placeholder="Số điện thoại (*)" />
                    </p>
                    <p class="col-md-12">
                        <input class="form-control" id="k_title" type="text" name="k_title" placeholder="Tên căn hộ (*)" />
                    </p>
                    <p class="col-md-6">
                        <select name="k_project" id="k_project" >
                            <?php if($q_du_an->have_posts()):while($q_du_an->have_posts()):$q_du_an->the_post(); ?>
                               <option value="<?php the_id(); ?>"><?php the_title(); ?></option>
                            <?php  
                                endwhile;
                                wp_reset_postdata();
                                endif;
                            ?> 
                        </select>
                    </p>
                    
                    <p class="col-md-6">
                        <select name="k_status"  id="k_status" >
                            <option>Có đồ</option>
                            <option>Không đồ</option>
                        </select>
                    </p>

                    
                    <p class="col-md-12">
                        <textarea class="form-control" id="k_content" name="k_content" placeholder="Thông tin thêm"></textarea>
                    </p> 
                     <p>
                        <span>&nbsp;</span> 
                        <button 
                            type="button" 
                            class="button-md uppercase to-right text-white soft-corners" 
                            id="k_submit">
                                <i class="fa fa-send-o">&nbsp;</i> 
                                Gửi
                            </button>
                    </p>
                </div>  
               
            </form> 
            <script type="text/javascript">
                jQuery(document).ready(function(){
                    jQuery("#k_submit").click(function(){
                        $("#k_result").html("<img src=\"<?php echo get_template_directory_uri(); ?>/includes/custom_ajax/loading.gif\" alt=\"loading\" />");
                         jQuery.ajax({
                            url: "<?php echo admin_url('admin-ajax.php') ?>",
                            type: "POST",
                            data: {
                                action: "post_new",
                                k_name: $("#k_name").val(),
                                k_phone: $("#k_phone").val(),
                                k_project: $("#k_project").val(),
                                k_title: $("#k_title").val(),
                                k_status: $("#k_status").val(),
                                k_content: $("#k_content").val(),
                            },
                            dataType: "html",
                            success: function(response) {
                                $("#k_result").html(response);
                                $("#frmKygui").reset();
                            }
                        });
                    }); 

                });

            </script>
    
                            
               
                <!-- /.row -->
            </div>
		<?php get_sidebar(); ?>
	</div>
	</div> <!-- /.container -->

</section>
<!-- /.box -->
<?php get_footer(); ?>
