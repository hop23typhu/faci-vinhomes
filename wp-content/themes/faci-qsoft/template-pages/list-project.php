<?php 
/*
 * template name:list project
*/
?>
<?php get_header(); ?>
<?php include(get_template_directory().'/multi-search.php'); ?>
<!-- /.book-form -->
<!-- main content -->
<section class="box">
    <div class="container">
	<div class="row">
		<div id="contLeft" class="col-md-9 col-sm-9">
                <div class="row">
                    <div class="col-md-12">
                          <div class="text-dark-blue text-center fancy-heading vc_custom_heading">
                                <h1>
                                 <?php the_title(); ?>
                                </h1><hr class="center-me" style="color: inherit; width:30%;">
                            </div>
                    
                    </div>
                </div>
                <!-- /.row -->
                <ul class="clean-list rooms-items row">
                    <?php  
                        $q_du_an= new WP_Query(
                            array(
                                'post_type'=>'project',
                            )
                        );
                        if($q_du_an->have_posts()):while($q_du_an->have_posts()):$q_du_an->the_post();
                    ?>
                    <li class="col-md-4 col-sm-6">
                        <div class="box-rooms">
                            <figure>
                                <a href='<?php the_permalink(); ?>'>
                                    <?php 
                                        if(has_post_thumbnail( ))
                                            the_post_thumbnail('large',array('alt'=>get_the_title()));
                                        else echo ' <img src="'.get_theme_mod("img_error").'" alt="image" />';
                                    ?>
                                </a>
                            </figure>
                            <div class="rooms-description">
                                <h3 class="title-rooms"><a href="<?php the_permalink(); ?>">
                                    <?php the_title(); ?>
                                </a></h3>
                                <p>
                                    <?php the_faci_excerpt(); ?>
                                </p>
                            </div>
                        </div>
                    </li>

                    <?php  
                        endwhile;
                        wp_reset_postdata();
                        endif;
                    ?> 
                            
                </ul>
                <!-- /.row -->
                <div class="clearfix pageNav">
                  <?php wp_pagenavi(array('query'=>$q_du_an)); ?>
                </div>
            </div>
		<?php get_sidebar(); ?>
	</div>
	</div> <!-- /.container -->

</section>
<!-- /.box -->
<?php get_footer(); ?>
