<?php 
/*
 * template name:contact
*/
?>
<?php get_header(); ?>
<?php include(get_template_directory().'/multi-search.php'); ?>
<!-- /.book-form -->
<!-- main content -->
<section class="box">
    <div class="container">
	<div class="row">
		<div id="contLeft" class="col-md-9 col-sm-9">
                <div class="row">
                    <div class="col-md-12">
                          <div class="text-dark-blue text-center fancy-heading vc_custom_heading">
                                <h1>
                                 <?php the_title(); ?>
                                </h1><hr class="center-me" style="color: inherit; width:30%;">
                            </div>
                    
                    </div>
                </div>
                <!-- /.row -->
                
                <?php 
                	if(have_posts()):while(have_posts()):the_post();
                		the_content();
                    endwhile;
                	endif;

                ?>
                            
               
                <!-- /.row -->
            </div>
		<?php get_sidebar(); ?>
	</div>
	</div> <!-- /.container -->

</section>
<!-- /.box -->
<?php get_footer(); ?>
