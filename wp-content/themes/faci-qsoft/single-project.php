<?php get_header(); ?>
<?php include(get_template_directory().'/multi-search.php'); ?>
<!-- /.book-form -->
<!-- main content -->
<section class="box">
    <div class="container">
	<div class="row">
		<div id="contLeft" class="col-md-9 col-sm-9">
            <?php  
                  if(have_posts()):while(have_posts()):the_post();
              ?>
			<div class="row">
				<div class="col-md-12">
				      <h1 itemprop="name" class="text-center text-dark-blue"> <?php the_title(); ?></asp:Literal></h1>
					<div class="box">
						<p style="text-align: center;">
                             <?php 
                                if(has_post_thumbnail( )) the_post_thumbnail('full',array('alt'=>get_the_title(),'style'=>'width: 800px; height: 443px;'));
                            ?>
                        </p>
                        <?php the_content(); ?>
					</div>
				</div>
			</div> <!-- /.row -->
             <div class="text-dark-blue text-center fancy-heading vc_custom_heading">
                <h1>
                    Danh sách căn hộ/biệt thự
                </h1><hr class="center-me" style="color: inherit; width:30%;">
            </div>
            <?php 
                    $arrID=get_post_meta(get_the_id(),'cf_can_ho',false) ;
                    $query_canho= new WP_Query(array('post__in'=>$arrID[0],'post_type'=>'house'));
                    if($query_canho->have_posts()): 
                ?>
    			<ul class="clean-list rooms-items row">
    				<?php  while($query_canho->have_posts()):$query_canho->the_post();?>
    				<li class="col-md-4 col-sm-6">
    					<div class="box-rooms">
    						<figure>
    							  <a href='<?php the_permalink(); ?>'>
                                    <?php 
                                        if(has_post_thumbnail( ))
                                            the_post_thumbnail('large',array('alt'=>get_the_title()));
                                        else echo ' <img src="'.get_theme_mod("img_error").'" alt="image" />';
                                    ?>
                                    </a>
    						</figure>
    						<div class="rooms-description">
    							<h3 class="title-rooms">
    							    <a href='<?php the_permalink(); ?>'>
                                        <?php the_title(); ?>
                                    </a>
    							</h3>
                                  <p>
                                   <?php the_faci_excerpt(); ?>
                                </p>
    						</div>
    					</div>
    				</li>
    				<?php endwhile; ?>
    			</ul><!-- /.row -->
            <?php  
                    wp_reset_postdata();
                    endif;
                endwhile;
                endif;
            ?> 
		</div><!-- /#contLeft -->
		<?php get_sidebar(); ?>
	</div>
	</div> <!-- /.container -->

</section>
<!-- /.box -->
<?php get_footer(); ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               