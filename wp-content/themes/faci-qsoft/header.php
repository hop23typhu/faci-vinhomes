<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
  <base href="<?php bloginfo('wpurl'); ?>/" >
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Fonts-->
     <link href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/vinhome/bootstrap.css"  />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/vinhome/bootstrap-responsive.css"  />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/vinhome/screen.css"  />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/vinhome/style.css"  />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
    <script>
            $(document).ready(function(){
                $(".mobile-switch").toggle(
                    function(){
                        $("body").addClass('acitve-mobile');
                    },
                    function(){
                        $("body").removeClass('acitve-mobile');
                    }
                ); 
            });
        </script>
    <script type="text/javascript">
         function checkWidth(init)
        {
            /*If browser resized, check width again */
            if ($(window).width() < 992) {
                $('.main-nav').addClass('mobile-menu');
            }
            else {
                if (!init) {
                    $('.main-nav').removeClass('mobile-menu');
                }
            }
        }

        
        $(document).ready(function() {
            checkWidth(true);

            $(window).resize(function() {
                checkWidth(false);
            });
            
            
        });
    </script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
    </head>
<body <?php body_class(); ?>>
    <form name="form1" method="post" id="form1">
<div>
</div>


<div>
</div>

    <?php if(is_home() || is_front_page() ): ?>
    <div class="modal" id="home-modal">
        <div class="modal-content">
            <a class="button-close" href="javascript:void(0)"><i class="fa fa-close"></i></a>
            <a href="<?php echo get_option('link-image-popup'); ?>"  target='_self'><img style="max-width:350px" src="<?php echo get_option('site-popup'); ?>"  alt='Voucher 2 ngày 1 đêm popup'   border='0'/></a>
        </div>
    </div>
    <?php endif; ?>
    <div id="home" class="boxed-view">
        <header class="main-header clearfix">
              <!-- Header Shorcode Area -->
              <div class="header-bar">
                <div class="container">
                  <div class="row">
                    <div class="col-md-8 col-sm-8">
                    
                                <ul class="inline-list uppercase font-small header-meta">
                                  <li><?php echo print_option("contact-address"); ?></li>
                                  <li><?php echo print_option("contact-phone"); ?></li>
                                </ul>

                    </div>
                                <div class="pull-right">
                        
                      <div class="to-right header-search">
                           <input name="q" type="text"  placeholder="Tìm kiếm" />
                      </div>
                    </div>
                  </div>
                </div>
               </div><!-- /.header-bar -->

                <div class="nav-bar sticky-bar white">
                    <!-- .mega-menu helper class ued as switcher -->
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 col-xs-9">
              <!-- Logo Area -->
              <figure class="identity">
                                <a href="<?php bloginfo('wpurl'); ?>"  target='_self'><img width="166px" src="<?php echo print_option('site-logo'); ?>"  alt='Logo'   border='0'/></a>
              </figure>
            </div><!-- /.col-md-2 -->
                            
                            <div class="col-md-9 col-xs-3">
                                <!-- Menu Area -->
                                <nav class="main-nav clearfix">
                                    <?php 
                                        $args = array(
                                            'theme_location' => 'primary-menu',
                                            'container' => '',
                                            'items_wrap' => '<ul id = "%1$s" class = "%2$s clean-list to-right">%3$s</ul>',
                                        );
                                        wp_nav_menu( $args );
                                    ?>
                                </nav>
                                <!-- /.main-nav -->

                                <a href="#" class="mobile-switch to-right">
                                    <i class="fa fa-bars font-2x text-dark hover-text-grey"></i>
                                </a>
                            </div>
                            <!-- /.col-md-10 -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container -->
                </div>
                <!-- /.nav-bar -->
            </header>
            <!-- /.main-nav -->