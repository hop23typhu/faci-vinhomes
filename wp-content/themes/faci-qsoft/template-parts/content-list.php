<li class="col-md-4 col-sm-6">
    <div class="box-rooms">
        <figure>
            <a href='<?php the_permalink(); ?>'>
                <?php 
                    if(has_post_thumbnail( ))
                        the_post_thumbnail('large',array('alt'=>get_the_title()));
                    else echo ' <img src="'.get_theme_mod("img_error").'" alt="image" />';
                ?>
            </a>
        </figure>
        <div class="rooms-description">
            <h3 class="title-rooms"><a href="<?php the_permalink(); ?>">
                <?php the_title(); ?>
            </a></h3>
            <p>
                <?php the_faci_excerpt(); ?>
            </p>
        </div>
    </div>
</li>