<?php get_header(); ?>
<?php include(get_template_directory().'/multi-search.php'); ?>
<!-- /.book-form -->
<!-- main content -->
<section class="box">
      <?php  
        if(have_posts()):while(have_posts()):the_post();
      ?>
    <div class="container">
        <div class="row">
          
            <div class="col-md-12">
                    <h1 itemprop="name" class="text-center text-dark-blue"> <?php the_title(); ?></h1>
                      <!-- Wrapper for slides -->
                   <div class="carousel slide article-slide" id="article-photo-carousel">
            <style type="text/css" media="screen">
                /* Main carousel style */
                .carousel {
                    width: 100%;
                }

                /* Indicators list style */
                .article-slide .carousel-indicators {
                    bottom: 0;
                    left: 0;
                    margin-left: 5px;
                    width: 100%;
                }
                /* Indicators list style */
                .article-slide .carousel-indicators li {
                    border: medium none;
                    border-radius: 0;
                    float: left;
                    height: 54px;
                    margin-bottom: 5px;
                    margin-left: 0;
                    margin-right: 5px !important;
                    margin-top: 0;
                    width: 100px;
                }
                /* Indicators images style */
                .article-slide .carousel-indicators img {
                    border: 2px solid #FFFFFF;
                    float: left;
                    height: 54px;
                    left: 0;
                    width: 100px;
                }
                /* Indicators active image style */
                .article-slide .carousel-indicators .active img {
                    border: 2px solid #428BCA;
                    opacity: 0.7;
                }
                            </style>
                            <script type="text/javascript">
                                $('.carousel').carousel({
                                  interval: false
                                });
                             </script>
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner cont-slider">
                    <?php if(get_field('cf_porfolio_image')): $i=0; ?>
                    <?php while(has_sub_field('cf_porfolio_image')): $i++; ?>
                    <?php 
                        $image_url= get_sub_field('cf_portfolio_image_page');
                    ?>
                    <div class="item <?php if($i==1) echo 'active'; ?>">
                      <img alt="" title="" src="<?php echo $image_url; ?>" style="width:100%">
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                  </div>
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <?php if(get_field('cf_porfolio_image')): $i=0; ?>
                    <?php while(has_sub_field('cf_porfolio_image')): $i++; ?>
                    <?php 
                        $image_url= get_sub_field('cf_portfolio_image_page');
                    ?>
                    <li class="<?php if($i==1) echo 'active'; ?>" data-slide-to="<?php echo $i-1; ?>"  data-target="#article-photo-carousel">
                      <img alt="" src="<?php echo $image_url; ?>">
                    </li>
                    <?php endwhile; ?>
                    <?php endif; ?>
                  </ol>
                </div>
          <?php the_content(); ?>
            </div>
        </div>
        <!-- /.row -->
       
    </div>
    <!-- /.container -->

<?php
    endwhile;
    endif;
?> 
</section>
<!-- /.box -->
<?php get_footer(); ?>