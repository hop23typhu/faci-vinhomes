<?php
/**
 * @package WordPress default theme
 * @subpackage Deeds theme of qsoft
 * @since deeds 1.0.2 (04/04/2016)
 */

add_action( 'after_setup_theme', 'deeds_start_setup' );
if ( ! function_exists( 'deeds_start_setup' ) ) {
	function deeds_start_setup() {
		global $themename, $shortname, $options, $theme_path, $theme_uri;
		$themename  = 'deeds';
		$shortname  = 'deeds';
		load_theme_textdomain( 'deeds', get_template_directory() . '/languages' );

		$theme_path = get_template_directory();
		$theme_uri  = get_template_directory_uri();

		add_image_size( 'blog-thumb', 720, 380, true );
		add_image_size( 'archive-thumb', 480, 310, true );
		add_image_size( 'single-thumb', 960, 480, true );
		add_image_size( 'testimonial-thumb', 80, 80, true );
		add_image_size( 'team-thumb', 400, 400, true );
		add_image_size( 'client-thumb', 170, 60, true );
		
		require_once( $theme_path . '/includes/install_plugins.php' );//install plugins theme need
		require_once( $theme_path . '/d_options/d-display.php' );//display option 
		require_once( $theme_path . '/d_options/d-custom.php' );
		require_once( $theme_path . '/d_options/inc/mailchimp/d-mailchimp.php' );
		require_once( $theme_path . '/includes/functions_mail.php' );//process about mail
		require_once( $theme_path . '/includes/functions_frontend.php' );
		require_once( $theme_path . '/includes/functions_init.php' );
		//require_once( $theme_path . '/includes/functions_styles.php' );//print option  Typography on head tag
		require_once( $theme_path . '/includes/functions_shortcodes.php' );// make shortcode for website . All shortcode here
		require_once( $theme_path . '/includes/function_ajax.php' );//process ajax 
		require_once( $theme_path . '/includes/custom-header.php' );
		require_once( $theme_path . '/includes/template-tags.php' );
		require_once( $theme_path . '/includes/extras.php' );
		require_once( $theme_path . '/includes/customizer.php' );//customizer in menu appearance
		require_once( $theme_path . '/includes/jetpack.php' );

		/** Create navigation **/
		if ( function_exists( 'wp_nav_menu') ) {
			add_theme_support( 'nav-menus' );
 
			register_nav_menus( array( 'primary-menu' => __( 'Menu chính website local', $themename ) ) );
			register_nav_menus( array( 'cat-menu' => __( 'Menu danh mục local', $themename ) ) );
			register_nav_menus( array( 'bot-menu' => __( 'Liên kết hữu ích local', $themename ) ) );
		}

		/** Create sidebar **/
		register_sidebar( array(
			'name' => __( 'Sidebar widgets', $themename ),
			'id' => 'sidebar-widgets',
			'description' => __( 'Sidebar widgets', $themename ),
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => ' <h3 class="widget-title">',
			'after_title' => '</h3>',
		) );
		register_sidebar( array(
			'name' => __( 'Left sidebar widgets', $themename ),
			'id' => 'sidebar-widgets-left',
			'description' => __( 'Sidebar widgets on left', $themename ),
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => '',
		) );
		register_sidebar( array(
			'name' => __( 'Footer column 1 widgets', $themename ),
			'id' => 'footer-column1-widgets',
			'description' => __( 'Footer column 1 widgets', $themename ),
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => '',
		) );
		register_sidebar( array(
			'name' => __( 'Footer column 2 widgets', $themename ),
			'id' => 'footer-column2-widgets',
			'description' => __( 'Footer column 2 widgets', $themename ),
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => '',
		) );
		register_sidebar( array(
			'name' => __( 'Footer column 3 widgets', $themename ),
			'id' => 'footer-column3-widgets',
			'description' => __( 'Footer column 3 widgets', $themename ),
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => '',
		) );
		register_sidebar( array(
			'name' => __( 'Footer copyright widgets', $themename ),
			'id' => 'footer-copyright-widgets',
			'description' => __( 'Footer copyright widgets', $themename ),
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => '',
		) );
		register_sidebar( array(
			'name' => __( 'Footer Subscribe widgets', $themename ),
			'id' => 'footer-subscribe-widgets',
			'description' => __( 'Footer subscribe widgets', $themename ),
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '<h4>',
			'after_title' => '</h4>',
		) );

		add_theme_support( 'automatic-feed-links' );//make feed link in head
		add_theme_support( 'title-tag' );//make auto title tag
		add_theme_support( 'html5', array(//support html5 tags 
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );
		add_theme_support( 'post-formats', array(//support some post format in post
			'aside',
			'image',
			'video',
			'quote',
			'link',
		) );
		add_theme_support( 'post-thumbnails' );//ddd post thumbnail
		//custom your template
		add_theme_support( 'custom-background', apply_filters( 'deeds_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );
	}
}
//unregister some widgets
function remove_default_widgets() {
     unregister_widget('WP_Widget_Pages');
     unregister_widget('WP_Widget_Calendar');
     unregister_widget('WP_Widget_Archives');
     unregister_widget('WP_Widget_Links');
     unregister_widget('WP_Widget_Meta');
     unregister_widget('WP_Widget_Search');
     unregister_widget('WP_Widget_Text');
     unregister_widget('WP_Widget_Recent_Posts');
     unregister_widget('WP_Widget_Recent_Comments');
     unregister_widget('WP_Widget_RSS');
}
add_action('widgets_init', 'remove_default_widgets', 11);
//Prevent hack SQL Injection 
global $user_ID; 
if($user_ID) {
	if(!current_user_can('administrator')) {
	        if (strlen($_SERVER['REQUEST_URI']) > 255 ||
	                stripos($_SERVER['REQUEST_URI'], "eval(") ||
	                stripos($_SERVER['REQUEST_URI'], "CONCAT") ||
	                stripos($_SERVER['REQUEST_URI'], "UNION+SELECT") ||
	                stripos($_SERVER['REQUEST_URI'], "base64"))
	                {
	                        @header("HTTP/1.1 414 Request-URI Too Long");
	                        @header("Status: 414 Request-URI Too Long");
	                        @header("Connection: Close");
	                        @exit;
	        		}
	}
}
//remove menubar (top) with user
if ( !current_user_can( 'manage_options' ) ) {
	add_filter( 'show_admin_bar', '__return_false' );
}



