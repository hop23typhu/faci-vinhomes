<?php get_header(); ?>
<?php include(get_template_directory().'/multi-search.php'); ?>
<!-- /.book-form -->
<!-- main content -->
<section class="box">
    <div class="container">
	<div class="row">
		<div id="contLeft" class="col-md-9 col-sm-9">
            <ul class="clean-list row news-post">
            <?php  
                if(have_posts()):while(have_posts()):the_post();
            ?>
               <li class="col-md-12 col-sm-12">
                   <div class="news-thumb">
                        <a href="<?php the_permalink(); ?>" >
                            <?php 
                                if(has_post_thumbnail( ))
                                    the_post_thumbnail('large',array('alt'=>get_the_title()));
                                else echo ' <img src="'.get_theme_mod("img_error").'" alt="image" />';
                            ?>
                        </a>
                    </div>
                   <div class="news-des">
                       <header class="blog-head">	
                        <h3>
                            <a href="<?php the_permalink(); ?>" >
                                <?php the_title(); ?>
                            </a>
                        </h3>
                           </header>
                       	<div class="blog-content"> 
                                <?php the_faci_excerpt(200); ?>
                               <p class="read-more-holder">
                                   <a class="read-more soft-corners text-dark button-sm hover-orange grey" href="<?php the_permalink(); ?>" >
                                   Xem chi tiết</a>
                               </p>
                        </div>
                    </div>
                </li>
                <?php  
                    endwhile;
                    endif;
                ?>
            </ul>
                        
			
				
			<div class="row">
				<div class="col-md-12">
					
        <div class="clearfix pageNav">
          <?php wp_pagenavi(); ?>
        </div>
				</div>
			</div> <!-- /.row -->
		</div><!-- /#contLeft -->
		<?php get_sidebar(); ?>
	</div>
	</div> <!-- /.container -->

            </section>
            <!-- /.box -->
<?php get_footer(); ?>