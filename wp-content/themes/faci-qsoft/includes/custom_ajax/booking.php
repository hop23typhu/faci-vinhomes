<?php  
add_action('wp_ajax_change_project', 'change_project');
add_action('wp_ajax_nopriv_change_project', 'change_project');
function change_project() {
	$id_project=$_POST['b_project'];
	$terms = get_the_terms( $id_project, 'house-type' );
	?>
	    <label>
	        LOẠI CĂN HỘ/BIỆT THỰ
	    </label>
	     <select name="b_type" id="b_type">
          <?php foreach($terms as $term) { ?>
          <option value="<?php echo $term->term_id ?>"><?php echo $term->name ?></option>
          <?php  } ?>
         </select>
	<?php
	   
	die();
}

add_action('wp_ajax_booking_form', 'booking_form');
add_action('wp_ajax_nopriv_booking_form', 'booking_form');
function booking_form() {
	 /*----------INSERT DATABASE----------*/
	$post = array(
	    'post_title'    => $_POST['b_username'],
	    'post_status'   => 'publish',
	    'post_type' => 'booking',
	);
	$ajax_booking_id = wp_insert_post($post);
    add_post_meta($ajax_booking_id, 'cf_booking_phone', $_POST['b_phone'], true);
    add_post_meta($ajax_booking_id, 'cf_booking_project', $_POST['b_project'], true);  
    add_post_meta($ajax_booking_id, 'cf_booking_type', $_POST['b_type'], true);  
    ?>
	<div class = "alert alert-success alert-dismissable">
	   <b>Đặt phòng thành công !</b>
	   <p>Cám ơn quý khách đã sử dụng dịch vụ của chúng tôi , chúng tôi sẽ liên hệ lại để xác nhận thông tin .</p
	</div>
    <?php
	die();
}