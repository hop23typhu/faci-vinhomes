<?php  
add_action('wp_ajax_post_new', 'post_new');
add_action('wp_ajax_nopriv_post_new', 'post_new');
function post_new() {
	/*----------VALIDATION----------*/
	//full name 
	if (isset($_POST['k_name']) & $_POST['k_name']!='') $k_name = $_POST['k_name']; else die(__("<p class='error alert alert-danger'>Bạn chưa nhập họ tên  !</p>","deeds"));
	//full name 
	if (isset($_POST['k_phone']) & $_POST['k_phone']!='') $k_phone = $_POST['k_phone']; else die(__("<p class='error alert alert-danger'>Bạn chưa nhập số điện thoại  !</p>","deeds"));
	//full name 
	if (isset($_POST['k_title']) & $_POST['k_title']!='') $k_title = $_POST['k_title']; else die(__("<p class='error alert alert-danger'>Bạn chưa nhập tên căn hộ  !</p>","deeds"));
	
	/*----------INSERT DATABASE----------*/
	$post = array(
	    'post_title'    => $k_name,
	    'post_content'    => $_POST['k_content'],
	    'post_status'   => 'publish',
	    'post_type' => 'kygui',
	);
	$ajax_kygui_id = wp_insert_post($post);
    add_post_meta($ajax_kygui_id, 'k_project', $_POST['k_project'], true);
    add_post_meta($ajax_kygui_id, 'k_phone', $k_phone, true);
    add_post_meta($ajax_kygui_id, 'k_title', $k_title, true);
    add_post_meta($ajax_kygui_id, 'k_status',$_POST['k_status'], true);
    /*----------SEND MAIL----------*/
    $body="<p style='font-weight:100;color:#444'>Họ tên : $k_name - SĐT : $_POST[k_phone] </p><p style='font-weight:100;color:#444'>Nội dung : $_POST[k_content] </p>";
    if( faci_send_mail( array( get_bloginfo('admin_email') ),"Ký gửi",$body) ) 
    die( '
    	<script type="text/javascript">
            $("#k_form").hide("fast");
        </script>    
        <p class="alert alert-success">'.get_option("mail-contact-success").'</p>
    ');
	else
	die( '
    	<p class="alert alert-danger">'.get_option("mail-contact-error").'</p>
    '); 
	
}