<?php



    
add_action('init', 'portfolio_register');  
  
function portfolio_register() {  
    global $themename;
    $labels = array(
        'name'               => __('Thư viện ảnh', 'post type general name', $themename),
        'singular_name'      => __('Thư viện ảnh', 'post type singular name', $themename),
        'parent_item_colon'  => ''
    );

    $args = array(  
        'labels'            => $labels,  
        'public'            => true,  
        'show_ui'           => true,
        'show_in_menu'      => true,
        'show_in_nav_menus' => false,
        'rewrite'           => false,
        'supports'          => array('title', 'editor', 'thumbnail'),
        'menu_icon'         => 'dashicons-format-gallery'
       );  
  
    register_post_type( 'portfolio' , $args );  
}

?>