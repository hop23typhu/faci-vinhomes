<?php
/** Testimonials **/
add_action( 'init', 'create_testimonials' );
function create_testimonials() {
    global $themename;
    $labels = array(
        'name'               => __('Cảm nhận', $themename),
        'singular_name'      => __('Cảm nhận của khách hàng', $themename),
        
    );
 
    register_post_type( 'testimonials', array(
        'labels'            => $labels,  
        'public'            => false,  
        'show_ui'           => true,
        'show_in_menu'      => true,
        'show_in_nav_menus' => false,
        'rewrite'           => false,
        'supports'          => array('title', 'editor','thumbnail'),
        'menu_icon'         => 'dashicons-format-chat'
    ) );
}
?>