<?php
/* Contact register post type */
    
add_action('init', 'contact_register');   
function contact_register() {  
    global $themename;
    $labels = array(
        'name'               => __('Liên hệ', 'post type general name', $themename),
        'singular_name'      => __('Liên hệ', 'post type singular name', $themename),
        'add_new'            => __('Thêm mới ', 'Contact', $themename),
        'add_new_item'       => __('Thêm mới liên hệ', $themename),
        'edit_item'          => __('Chỉnh sửa', $themename),
        'parent_item_colon'  => ''
    );

    $args = array(  
        'labels'            => $labels,  
        'public'            => false,// hidden in front-end
        'show_ui'           => true,
        'show_in_menu'      => true,
        'show_in_nav_menus' => false,
        'rewrite'           => false,
        'supports'          => array('title', 'editor'),
        'has_archive'       => true,
        'menu_icon'         => 'dashicons-email-alt'
       );  
  
    register_post_type( 'contact' , $args );  
}
/* Contact more info display */
add_filter('manage_edit-contact_columns', 'add_new_contact_columns');
function add_new_contact_columns($columns) {
    $columns['cb'] = '<input type="checkbox" />';
    $columns['title'] = _x('Title', 'column name');
    $columns['cemail'] = __('E-mail');
    $columns['cphone'] = __('Phone');
    $columns['date'] = __('Date');
    
    return $columns;
}
// Add to admin_init function
add_action('manage_contact_posts_custom_column', 'manage_contact_columns' ,10, 2);
function manage_contact_columns($column_name, $post_ID) {
    global $post;
    switch ($column_name) {
    case 'cemail':
        echo get_post_meta($post->ID, 'cemail',true);
        break;
     case 'cphone':
        echo get_post_meta($post->ID, 'cphone',true);
        break;
    default:
        break;
    } // end switch
} 
?>