<?php get_header(); ?>
<?php include(get_template_directory().'/multi-search.php'); ?>
<!-- /.book-form -->
<!-- main content -->
<section class="box">
    <div class="container">
  <div class="row">
    <div id="contLeft" class="col-md-9 col-sm-9">
      <?php  
          if(have_posts()):while(have_posts()):the_post();
      ?>
      <header class="fancy-heading blog-head">                
        <h2 style="font-size: 1.563em;">
            <?php the_title(); ?>
        </h2>
      </header>
      <div class="row">
          <div class="col-md-12">
            <article class="blog-item">
              <div class="blog-content">
                
                <?php the_content(); ?>

              </div>
            </article>
          </div>
        </div>
    <?php  
        endwhile;
        endif;
    ?>       
    </div><!-- /#contLeft -->
    <?php get_sidebar(); ?>
  </div>
  </div> <!-- /.container -->
</section>
<!-- /.box -->
<?php get_footer(); ?>