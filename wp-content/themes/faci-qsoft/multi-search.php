<?php  
    $q_du_an= new WP_Query(
        array(
            'post_type'=>'project',
            'posts_per_page'=>4
        )
    );
?>
<section class="box search-roms <?php if(!is_home() && !is_front_page() ): _e('second_search');  endif; ?>  ">
<div class="container">
    <div class="booking-form" id="form-booking">
        <?php if(is_home() || is_front_page() ): _e('<h4 id="UcBookingEngineLink_h4_Title" class="the-title"> Đặt Phòng</h4>');  endif; ?>
            <div class="row">
                <div class="col-md-2 col-sm-6">
                    <label>
                        Họ và tên
                    </label>
                    <input   name="b_username" id="b_username" type="text" placeholder="Nhập họ tên của bạn ">
                    
                </div>
                <div class="col-md-2 col-sm-6">
                    <label>
                        Số điện thoại
                    </label>
                    <input   name="b_phone" id="b_phone" type="text" placeholder="Nhập số điện thoại của bạn ">
                </div>
       
                      <div class="col-md-3 col-sm-12">
                    <label>
                        KHU CĂN HỘ/BIỆT THỰ
                    </label>
                       <select name="b_project" id="b_project" >
                            <option value="">-------Chọn dự án-------</option>
                            <?php if($q_du_an->have_posts()):while($q_du_an->have_posts()):$q_du_an->the_post(); ?>
                               <option value="<?php the_id(); ?>"><?php the_title(); ?></option>
                            <?php  
                                endwhile;
                                wp_reset_postdata();
                                endif;
                            ?> 
                        </select>
                </div>
                <div class="col-md-3 col-sm-12" id="area_type">
                    <label>
                        LOẠI CĂN HỘ/BIỆT THỰ
                    </label>
                      
                  <select name="b_type">
                      <option value="">-------Chọn loại căn hộ-------</option>
                  </select>
                </div>
                <div class="col-md-2 col-sm-12 text-right" >
                    <button type="button" id="b_submit" class="button-md soft-corners to-left"><i class="fa fa-search"></i>
                        Đặt phòng
                    </button>
                </div>
            </div>
            <script type="text/javascript">
                jQuery(document).ready(function(){  
                    jQuery("#b_submit").click(function() {   
                        var b_username = jQuery('#b_username').val();
                        var b_phone = jQuery('#b_phone').val();
                        var b_project = jQuery('#b_project').val();
                        var b_type = jQuery('#b_type').val();

                        if(b_username == ''){ jQuery('#b_username').css('border','1px solid red');  }
                        else if(b_phone == ''){ jQuery('#b_phone').css('border','1px solid red');  }
                        else if(b_project == ''){ jQuery('#b_project').css('border','1px solid red');  }

                        if(b_username != ''){ jQuery('#b_username').css('border','0px solid red');  }
                        if(b_phone != ''){ jQuery('#b_phone').css('border','0px solid red');  }
                        if(b_project != ''){ jQuery('#b_project').css('border','0px solid red');  }   
                        if(b_username !='' && b_phone != '' && b_username != '')    {
                            jQuery.ajax({
                                type: "POST",
                                url: "<?php echo admin_url('admin-ajax.php'); ?>",
                                data: {
                                    action: "booking_form",
                                    b_username: b_username,
                                    b_phone: b_phone,
                                    b_project: b_project,
                                    b_type: b_type,
                                },
                                success: function(response) {                                    
                                   jQuery('#form-booking').html(response);
                                }

                            }); 
                            return false;
                        }
                    }); 



                    jQuery("#b_project").change(function(e) {   
                        var b_project = jQuery('#b_project').val();
                        jQuery.ajax({
                            type: "POST",
                            url: "<?php echo admin_url('admin-ajax.php'); ?>",
                            data: {
                                action: "change_project",
                                b_project: b_project,
                            },
                            success: function(response) {                                    
                               jQuery('#area_type').html(response);
                            }

                        }); 
                        return false;
                    });
                });

            </script>
      
    </div>
</div>

</section>